import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule} from '@angular/http';
import { FormsModule } from '@angular/forms';
import { EditorModule } from '@tinymce/tinymce-angular';


import { ModuloEmailModule} from './modules/moduloemail.module';
import { AdminModule } from './modules/admin/admin.module';

import { AppComponent } from './app.component';
import { AnimalesComponent } from './components/animales.component';
import { ContactoComponent } from './components/contacto.component';
import { CuidadoresComponent } from './components/cuidadores.component';
import { HomeComponent } from './components/home.component';
import { TiendaComponent } from './components/tienda.component';
import { routing, appRoutingProviders } from './app.routing';
import { ErrorComponent } from './components/error.component';


@NgModule({
  declarations: [
    AppComponent,
    AnimalesComponent,
    ContactoComponent,
    CuidadoresComponent,
    HomeComponent,
    TiendaComponent,
    ErrorComponent,
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    EditorModule,
    ModuloEmailModule,
    AdminModule
    

  ],
  providers: [appRoutingProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
