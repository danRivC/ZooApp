import { Component } from '@angular/core';

@Component({
    selector: 'cuidadores',
    templateUrl: '../views/cuidadores.html'
})
export class CuidadoresComponent{
    public titulo:string;
    constructor(){
        this.titulo='Cuidadores';
    }
}