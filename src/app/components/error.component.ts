import { Component } from '@angular/core';

@Component({
    selector: 'error',
    templateUrl: '../views/error.html'
})
export class ErrorComponent{
    public titulo: string;

    constructor(){
        this.titulo='Lo siento pero la pagina que buscas no existes'
    }
}