import { Component } from '@angular/core';

@Component({
    selector: 'animales',
    templateUrl: '../views/animales.html'
})
export class AnimalesComponent{
    public titulo:string;
    constructor(){
        this.titulo='Animales'
    }
}