import { Component, OnInit } from '@angular/core';
import { EditorModule } from '@tinymce/tinymce-angular';

import * as $ from 'jquery';



@Component({
    selector: 'tienda',
    templateUrl: '../views/tienda.html'
})
export class TiendaComponent{
    public titulo: string;
    constructor(){
        this.titulo='Bienvenido a la Tienda';
    }
    ngOnInit(){
        $('#textojq').removeClass('hidden');
        $('#botonjq').click(function(){
            console.log('Click desde jquery');
            $('#textojq').slideToggle();
        });
        
    }
}