//Importar modulos necesarios para la creación de modulos
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';


//Importar Componentes 
import { GuardarEmailComponent } from './email/componentes/guardar-email/guardar-email.component';
import { MostrarEmailComponent } from './email/componentes/mostrar-email/mostrar-email.component';
import { MainEmailComponent } from './email/componentes/main-email/main-email.component';

//Generar Nuevo Modulo 
@NgModule({
    imports:[CommonModule,
        FormsModule
    ],
    declarations:[
        GuardarEmailComponent,
        MostrarEmailComponent,
        MainEmailComponent
    ],
    exports: [MainEmailComponent]

})
export class ModuloEmailModule{ }