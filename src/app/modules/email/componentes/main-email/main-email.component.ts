import { Component, DoCheck, OnInit } from '@angular/core';

@Component({
    selector : 'main-email',
    template :`
        <div class="panel panel-default">
            <h2>{{titulo}}</h2>
            <hr>
            <guardar-email></guardar-email>
            <mostrar-email></mostrar-email>
        </div>
    `
    
}) 
export class MainEmailComponent implements OnInit{
    public titulo = 'Modulo de Emails';
    public emailContacto: string;
    
    guardarEmail(){
        localStorage.setItem('emailContacto',this.emailContacto);
    }
    ngOnInit(){
        console.log('Componente de los modulos cargado')
    }

}