import { Component, DoCheck, OnInit } from '@angular/core';

@Component({
    selector : 'guardar-email',
    template : `
            <h4>{{titulo}}</h4>
            <input type="text" [(ngModel)]="emailContacto" />
            <button (click)="guardarEmail()">Guardar Email</button>
    `
    
}) 
export class GuardarEmailComponent{
    public titulo = 'Guardar Email';
    public emailContacto: string;
    
    guardarEmail(){
        localStorage.setItem('emailContacto',this.emailContacto);
    }

}