import { Component } from '@angular/core';

@Component({
    selector: 'admin-main',
    templateUrl: '../views/main.html' 
})
export class MainComponent{
    public titulo:string;
    constructor(){
        this.titulo='Panel de Administracion'
    }

}