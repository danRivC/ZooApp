import { Component } from '@angular/core';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AdminRoutingModule } from './admin-routing.module';


//Componentes
import { AddComponent } from './components/add.component';
import { MainComponent } from './components/main.component';
import { ListComponent } from './components/list.component';
import { EditComponent } from './components/edit.component';

@NgModule({
    declarations: [
        MainComponent,
        ListComponent,
        AddComponent,
        EditComponent
    ],
    imports:[
        CommonModule,
        FormsModule,
        HttpModule,
        AdminRoutingModule
    ],
    exports: [MainComponent,
        ListComponent,
        AddComponent,
        EditComponent],
    providers: []
})
export class AdminModule{ }




