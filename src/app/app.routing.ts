import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';



//Componentes Creados
import { HomeComponent } from './components/home.component';
import { ContactoComponent } from './components/contacto.component';
import { CuidadoresComponent } from './components/cuidadores.component';
import { AnimalesComponent } from './components/animales.component';
import { TiendaComponent } from './components/tienda.component';
import { ErrorComponent } from './components/error.component';


const appRoutes: Routes=[
    {path: '', component:HomeComponent},
    {path: 'home', component:HomeComponent},
    {path: 'contacto', component:ContactoComponent},
    {path: 'cuidadores', component:CuidadoresComponent},
    {path: 'animales', component:AnimalesComponent},
    {path: 'tienda', component:TiendaComponent},
    {path: '**', component:ErrorComponent}

]

export const appRoutingProviders : any[] =  [];
export const routing : ModuleWithProviders = RouterModule.forRoot(appRoutes);   